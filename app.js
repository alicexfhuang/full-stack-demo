var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var request = require('then-request');
var mongoose = require("mongoose");
// var FormData = new FormData();
var axios = require("axios");

mongoose.connect("mongodb://localhost/hotel_app");

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");

var hotelSchema = new mongoose.Schema({
  id: Number,
  hotel_name: String,
  num_reviews: Number,
  address: String,
  num_stars: Number,
  amenities: [{
    type: String
  }],
  image_url: String,
  price: Number,
  retail_price: {type: Number, default: -1}
});

var Hotel = mongoose.model("Hotel", hotelSchema);

app.get("/", function(req, res) {
  res.render("form");
});

app.get("/commonHotels", function(req, res) {
  Hotel.find({retail_price: {$ne: -1}}, function(err, hotels) {
    if (err) {
      console.log("error");
    } else {
      res.render("display", {hotels: hotels});
    }
  })
})

var uri = 'https://experimentation.getsnaptravel.com/interview/hotels';

app.post("/hotels", function(req, res) {

  axios({
    method: 'post',
    url: uri,
    data: {
      city: req.body.city,
      checkin: req.body.checkin,
      checkout: req.body.checkout,
      provider: 'snaptravel'
    }
  }).then(function (res) {
    var hotels = res.data.hotels;
    for (var i = 0; i < hotels.length; i++) {
      Hotel.create(hotels[i], function(err, newHotel) {
        if (err) {
          console.log("error");
        }
      });
    }
  }).then(() => {
    axios({
      method: 'post',
      url: uri,
      data: {
        city: req.body.city,
        checkin: req.body.checkin,
        checkout: req.body.checkout,
        provider: 'retail'
      }
    }).then((res) => {
      var hotels = res.data.hotels;
      for (var i = 0; i < hotels.length; i++) {
        var retailPrice = hotels[i].price;
        var id = hotels[i].id;
        Hotel.findOne({'id': id}, function(err, foundHotel) {
          if (err) {
            console.log("error");
          } else {
            Hotel.findOneAndUpdate({'id': id}, {$set: {'retail_price': retailPrice}}, function(err, foundHotel) {
              if (err) {
                console.log("error");
              }
            });
          }
        })
      }
    }).then(() => {
      res.redirect("/commonHotels");
    });
  });
});



app.listen(3000, function() {
  console.log("App is running on port 3000...")
});
